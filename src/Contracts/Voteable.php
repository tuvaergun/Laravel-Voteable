<?php

/*
 * This file is part of Laravel Voteable.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DraperStudio\Voteable\Contracts;

/**
 * Interface Voteable.
 */
interface Voteable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function votes();
}
