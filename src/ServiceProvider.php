<?php

/*
 * This file is part of Laravel Voteable.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DraperStudio\Voteable;

use DraperStudio\ServiceProvider\ServiceProvider as BaseProvider;

class ServiceProvider extends BaseProvider
{
    protected $packageName = 'voteable';

    public function boot()
    {
        $this->setup(__DIR__)
             ->publishMigrations();
    }
}
